/*
 * Copyright (C) 2014
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static inline void at_buf_init(struct at_buffer *b, struct at_entry *buf, size_t size)
{
	b->buffer = buf;
	b->size   = size;
	b->used   = 0;
	b->read   = 0;
	b->write  = 0;
}

static inline void at_buf_reset(struct at_buffer *b)
{
	b->used   = 0;
	b->read   = 0;
	b->write  = 0;
}

static inline size_t at_buf_used(struct at_buffer *b)
{
	return b->used;
}

static inline size_t at_buf_free(struct at_buffer *b)
{
	return b->size - b->used;
}

// ============================ READ ============================= //

static struct at_entry *at_buf_read(struct at_buffer *b)
{
	if (b->used > 0)
	{
		return &b->buffer[b->read];
	}

	return NULL;
}


static void at_buf_read_finish(struct at_buffer *b)
{
	size_t s;

	if (b->used > 0)
	{
		b->used -= 1;

		if (b->used == 0)
		{
			b->read  = 0;
			b->write = 0;
		}
		else
		{
			s = b->read + 1;

			if (s >= b->size)
			{
				b->read = s - b->size;
			}
			else
			{
				b->read = s;
			}
		}
	}
}

// ============================ WRITE ============================ //

static struct at_entry *at_buf_write(struct at_buffer *b)
{
	if (at_buf_free(b) > 0)
	{
		return &b->buffer[b->write];
	}

	return NULL;
}

static void at_buf_write_finish(struct at_buffer *b)
{
	size_t s;

	if (at_buf_free(b) > 0)
	{
		s = b->write + 1;

		if (s >= b->size)
		{
			b->write = s - b->size;
		}
		else
		{
			b->write = s;
		}

		b->used += 1;
	}
}
