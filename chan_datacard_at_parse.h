/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static char	*at_parse_cnum	(char *, size_t);
static char	*at_parse_cops	(char *, size_t);
static int	at_parse_creg	(char *, size_t, int *, int *, char **, char **);
static char	*at_parse_clip	(char *, size_t);
static int	at_parse_cusd	(char *, size_t, int *, char **, int *);
static int	at_parse_cmgr	(char *, size_t, char **, char **);
