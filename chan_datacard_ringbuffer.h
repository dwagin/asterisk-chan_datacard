/*
 * Copyright (C) 2009
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

#ifndef __RINGBUFFER_H__
#define __RINGBUFFER_H__

#include <sys/uio.h>
#include <string.h>

struct ringbuffer
{
	void	*buffer;
	size_t	size;
	size_t	used;
	size_t	read;
	size_t	write;
};

static inline void	rb_init			(struct ringbuffer *, void *, size_t);
static inline void	rb_reset		(struct ringbuffer *);

static inline size_t	rb_used			(struct ringbuffer *);
static inline size_t	rb_free			(struct ringbuffer *);

static int		rb_read_all_iov		(struct ringbuffer *, struct iovec *);
static int		rb_read_n_iov		(struct ringbuffer *, struct iovec *, size_t);
static size_t		rb_read_upd		(struct ringbuffer *, size_t);

static size_t		rb_write		(struct ringbuffer *, void *, size_t);

#endif /* __RINGBUFFER_H__ */
