/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_write		(const char *, int, const char *, size_t);

static int at_send_entry	(struct datacard_pvt *);
static int at_send_buf		(struct datacard_pvt *, at_command_t, at_response_t, const char *, size_t);

static int at_send_at		(struct datacard_pvt *);
static int at_send_atz		(struct datacard_pvt *);
static int at_send_ate0		(struct datacard_pvt *);
static int at_send_cmee		(struct datacard_pvt *);

static int at_send_cgmi		(struct datacard_pvt *);
static int at_send_cgmm		(struct datacard_pvt *);
static int at_send_cgmr		(struct datacard_pvt *);
static int at_send_cgsn		(struct datacard_pvt *);

static int at_send_u2diag0	(struct datacard_pvt *);

static int at_send_cimi		(struct datacard_pvt *);
static int at_send_cpin_test	(struct datacard_pvt *);
static int at_send_cnum		(struct datacard_pvt *);
static int at_send_clip		(struct datacard_pvt *);

static int at_send_cops_init	(struct datacard_pvt *);
static int at_send_cops		(struct datacard_pvt *);
static int at_send_creg_init	(struct datacard_pvt *);
static int at_send_creg		(struct datacard_pvt *);
static int at_send_stsf		(struct datacard_pvt *);

static int at_send_cvoice_test	(struct datacard_pvt *);
static int at_send_cssn		(struct datacard_pvt *);

static int at_send_cmgf		(struct datacard_pvt *);
static int at_send_cpms		(struct datacard_pvt *);
static int at_send_cscs_ucs2	(struct datacard_pvt *);
static int at_send_cnmi		(struct datacard_pvt *);

static int at_send_curc		(struct datacard_pvt *);

static int at_send_csq		(struct datacard_pvt *);

static int at_send_ata		(struct datacard_pvt *);
static int at_send_clir		(struct datacard_pvt *, int);
static int at_send_atd		(struct datacard_pvt *, const char *);
static int at_send_ddsetex	(struct datacard_pvt *);
static int at_send_clvl		(struct datacard_pvt *, int);
static int at_send_chup		(struct datacard_pvt *);
static int at_send_dtmf		(struct datacard_pvt *, char);

static int at_send_cusd		(struct datacard_pvt *, const char *);

static int at_send_cmgr		(struct datacard_pvt *, int);
static int at_send_cmgd		(struct datacard_pvt *, int, int);

static int at_send_ccwa_disable	(struct datacard_pvt *);
static int at_send_cfun_reboot	(struct datacard_pvt *);
static int at_send_cpin		(struct datacard_pvt *, const char *);
static int at_send_clck		(struct datacard_pvt *, const char *);
