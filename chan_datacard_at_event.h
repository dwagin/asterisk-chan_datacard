/* 
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_event		(struct datacard_pvt *, char *, size_t);

static int at_event_rssi	(struct datacard_pvt *, char *);
static int at_event_mode	(struct datacard_pvt *, char *);

static int at_event_cend	(struct datacard_pvt *, char *);
static int at_event_conf	(struct datacard_pvt *, char *);
static int at_event_conn	(struct datacard_pvt *);

static int at_event_clip	(struct datacard_pvt *, char *, size_t);

static int at_event_cusd	(struct datacard_pvt *, char *, size_t);
static int at_event_cmti	(struct datacard_pvt *, char *, size_t);
static int at_event_smmemfull	(struct datacard_pvt *);
