/*
 * chan_datacard
 *
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 */

/*! \file
 *
 * \brief UMTS Voice Datacard channel driver
 *
 * \author Artem Makhutov <artem@makhutov.org>
 * \author Dmitry Wagin <dmitry.wagin@ya.ru>
 *
 * \ingroup channel_drivers
 */

#include <asterisk.h>

#include <asterisk/app.h>
#include <asterisk/astdb.h>
#include <asterisk/callerid.h>
#include <asterisk/causes.h>
#include <asterisk/channel.h>
#include <asterisk/cli.h>
#include <asterisk/dsp.h>
#include <asterisk/format_cache.h>
#include <asterisk/frame.h>
#include <asterisk/linkedlists.h>
#include <asterisk/manager.h>
#include <asterisk/module.h>
#include <asterisk/musiconhold.h>
#include <asterisk/pbx.h>
#include <asterisk/poll-compat.h>
#include <asterisk/stasis_channels.h>
#include <asterisk/stringfields.h>
#include <asterisk/timing.h>

#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#include <iconv.h>

#include "chan_datacard_version.h"

#include "chan_datacard_at_buffer.h"
#include "chan_datacard_ringbuffer.h"

#include "chan_datacard.h"

#include "chan_datacard_at_event.h"
#include "chan_datacard_at_parse.h"
#include "chan_datacard_at_read.h"
#include "chan_datacard_at_response.h"
#include "chan_datacard_at_send.h"
#include "chan_datacard_channel.h"
#include "chan_datacard_char_conv.h"
#include "chan_datacard_cli.h"
#include "chan_datacard_helpers.h"
#include "chan_datacard_locks.h"
#include "chan_datacard_manager.h"

#include "chan_datacard_at_buffer.c"
#include "chan_datacard_at_event.c"
#include "chan_datacard_at_parse.c"
#include "chan_datacard_at_read.c"
#include "chan_datacard_at_response.c"
#include "chan_datacard_at_send.c"
#include "chan_datacard_channel.c"
#include "chan_datacard_char_conv.c"
#include "chan_datacard_cli.c"
#include "chan_datacard_helpers.c"
#include "chan_datacard_manager.c"
#include "chan_datacard_ringbuffer.c"

static const char config[] = "datacard.conf";

static void pvt_reset(struct datacard_pvt *pvt)
{
	pvt->data_fd		= -1;
	pvt->audio_fd		= -1;
	pvt->event_fd		= -1;

	pvt->connected		= 0;
	pvt->initialized	= 0;
	pvt->gsm_registered	= 0;
	pvt->incoming		= 0;
	pvt->outgoing		= 0;

	pvt->ringing		= 0;
	pvt->talking		= 0;

	pvt->needchup		= 0;

	pvt->rssi		= -1;
	pvt->linkmode		= -1;
	pvt->linksubmode	= -1;
	pvt->gsm_status		= -1;

	ast_string_field_set(pvt, provider,		NULL);
	ast_string_field_set(pvt, manufacturer,		NULL);
	ast_string_field_set(pvt, model,		NULL);
	ast_string_field_set(pvt, firmware,		NULL);
	ast_string_field_set(pvt, imei,			NULL);
	ast_string_field_set(pvt, imsi,			NULL);
	ast_string_field_set(pvt, number,		NULL);
	ast_string_field_set(pvt, location_area_code,	NULL);
	ast_string_field_set(pvt, cell_id,		NULL);

	at_buf_reset(&pvt->data_at_buf);
}

static int disconnect_datacard(struct datacard_pvt *pvt)
{
	if (pvt->connected)
	{
		pvt->connected = 0;
		manager_event_status(pvt);
	}

	if (pvt->needchup)
	{
		at_buf_reset(&pvt->data_at_buf);
		at_send_chup(pvt);
	}

	if (pvt->owner)
	{
		ast_debug(1, "[%s] Datacard disconnected, hanging up owner\n", pvt->id);
		channel_queue_hangup(pvt, 0);
	}

	ast_debug(5, "[%s] Close tty's\n", pvt->id);

	int data_fd  = pvt->data_fd;
	int audio_fd = pvt->audio_fd;
	int event_fd = pvt->event_fd;

	pvt_reset(pvt);

	pvt_unlock(pvt);

	tty_close(&data_fd);
	tty_close(&audio_fd);
	tty_close(&event_fd);

	pvt_lock(pvt);

	ast_debug(1, "[%s] Datacard has disconnected\n", pvt->id);
	ast_verb(4,  "[%s] Datacard has disconnected\n", pvt->id);

	return 0;
}

static void *datacard_monitor(void *data)
{
	struct datacard_pvt *pvt = (struct datacard_pvt *) data;
	struct at_entry	*e;
	struct timeval init;

	int fds[2];
	ssize_t res;
	char data_buf[2*1024];
	char event_buf[1024];
	size_t data_buf_used;
	size_t event_buf_used;
	char *s;
	char *sl;

	static const char return_newline[] = "\r\n";
	static const char exclusion1[] = "\r\n+CSSU: ";
	static const char exclusion2[] = "+CSSI: ";

	while (!check_unloading())
	{
		ast_debug(1, "[%s] Datacard trying to connect...\n", pvt->id);
		ast_verb(4,  "[%s] Datacard trying to connect...\n", pvt->id);

		pvt_lock(pvt);

		pvt->timeout = discovery_interval;

		pvt->data_fd  = tty_open(pvt->data_tty);
		pvt->audio_fd = tty_open(pvt->audio_tty);
		pvt->event_fd = tty_open(pvt->event_tty);

		if (pvt->data_fd < 0 || pvt->audio_fd < 0 || pvt->event_fd < 0)
		{
			goto e_cleanup;
		}

		ast_debug(1, "[%s] Datacard has connected, initializing...\n", pvt->id);
		ast_verb(4,  "[%s] Datacard has connected, initializing...\n", pvt->id);

		pvt->connected = 1;

		fds[0] = pvt->data_fd;
		fds[1] = pvt->event_fd;

		data_buf_used = 0;
		event_buf_used = 0;

		init = ast_tvadd(ast_tvnow(), ast_tv(30, 0));

		if (at_send_at(pvt))
		{
			goto e_cleanup;
		}

		while (!check_unloading())
		{
			pvt_unlock(pvt);
			res = at_wait_read(pvt->id, fds, 2, 2000);
			pvt_lock(pvt);

			if (res == 1)
			{
				res = at_read(pvt->id, "D", pvt->data_fd, data_buf, sizeof(data_buf));
				at_buf_reset(&pvt->data_at_buf);
			}
			else if (res == 2)
			{
				res = at_read(pvt->id, "E", pvt->event_fd, data_buf, sizeof(data_buf));
			}
			else
			{
				break;
			}

			if (res < 0)
			{
				goto e_cleanup;
			}
		}

		if (at_buf_read(&pvt->data_at_buf))
		{
			ast_log(LOG_WARNING, "[%s] Timeout waiting response to [AT]\n", pvt->id);
			pvt->timeout = 60;
			goto e_cleanup;
		}

		if (at_send_atz(pvt) || at_send_ate0(pvt) || at_send_cmee(pvt))
		{
			goto e_cleanup;
		}

		while (!check_unloading())
		{
			if (tty_status(pvt->audio_fd) || tty_status(pvt->data_fd) || tty_status(pvt->event_fd))
			{
				ast_log(LOG_WARNING, "[%s] Lost connection\n", pvt->id);
				goto e_cleanup;
			}

			pvt_unlock(pvt);
			res = at_wait_read(pvt->id, fds, 2, 10000);
			pvt_lock(pvt);

			if (!pvt->initialized)
			{
				if (ast_tvcmp(init, ast_tvnow()) < 0)
				{
					ast_log(LOG_WARNING, "[%s] Timeout initializing\n", pvt->id);
					pvt->timeout = 10;
					goto e_cleanup;
				}
			}

			if ((e = at_buf_read(&pvt->data_at_buf)) && ast_tvcmp(e->timeout, ast_tvnow()) < 0)
			{
				trimnr(e->buf, &e->len, "\r\n");
				ast_log(LOG_WARNING, "[%s] Timeout waiting response to [%.*s]\n", pvt->id, (int) e->len, e->buf);
				goto e_cleanup;
			}

			if (res == 1)
			{
				res = at_read(pvt->id, "D", pvt->data_fd, data_buf + data_buf_used, (sizeof(data_buf) - 1) - data_buf_used);

				if (res == 0)
				{
					continue;
				}
				else if (res < 0)
				{
					goto e_cleanup;
				}

				data_buf_used += res;

				if ((data_buf_used >= LEN(return_newline)) && !memcmp(data_buf + data_buf_used - LEN(return_newline), STR_AND_LEN(return_newline)))
				{
					data_buf[data_buf_used] = '\0';

					s = memmem(data_buf, data_buf_used, STR_AND_LEN(return_newline));

					if (s == NULL)
					{
						ast_log(LOG_ERROR, "[%s] Bad response format\n", pvt->id);
						goto e_cleanup;
					}

					s += LEN(return_newline);
					data_buf_used -= s - data_buf;

					if (data_buf_used == 0)
					{
						continue;
					}

					e = at_buf_read(&pvt->data_at_buf);

					if (e)
					{
						if (at_response(pvt, e, s, data_buf_used))
						{
							goto e_cleanup;
						}

						at_buf_read_finish(&pvt->data_at_buf);

						if (at_send_entry(pvt))
						{
							goto e_cleanup;
						}

						data_buf_used = 0;
					}
					else
					{
						ast_log(LOG_ERROR, "[%s] Response for no reason: [%.*s]\n",
							pvt->id, (int) (data_buf_used - LEN(return_newline)), s);
						goto e_cleanup;
					}

				}
				else if (data_buf_used == (sizeof(data_buf) - 1))
				{
					ast_log(LOG_ERROR, "[%s] Data read buffer overflow\n", pvt->id);
					goto e_cleanup;
				}
			}
			else if (res == 2)
			{
				res = at_read(pvt->id, "E", pvt->event_fd, event_buf, (sizeof(event_buf) - 1));

				if (res == 0)
				{
					continue;
				}
				else if (res < 0)
				{
					goto e_cleanup;
				}

				event_buf_used = res;
				s = event_buf;

				while (event_buf_used)
				{
					if ((event_buf_used >= LEN(return_newline)) && memcmp(s, STR_AND_LEN(return_newline)))
					{
						ast_log(LOG_ERROR, "[%s] Bad event format\n", pvt->id);
						goto e_cleanup;
					}

					s += LEN(return_newline);
					event_buf_used -= LEN(return_newline);

					if ((event_buf_used > LEN(exclusion1)) && !memcmp(s, STR_AND_LEN(exclusion1)))
					{
						continue;
					}
					else if ((event_buf_used > LEN(exclusion2)) && !memcmp(s, STR_AND_LEN(exclusion2)))
					{
						sl = s + LEN(exclusion2) + 1;

						if (at_event(pvt, s, sl - s))
						{
							goto e_cleanup;
						}
					}
					else
					{
						sl = memmem(s, event_buf_used, STR_AND_LEN(return_newline));

						if (sl == NULL)
						{
							ast_log(LOG_ERROR, "[%s] Bad event format\n", pvt->id);
							goto e_cleanup;
						}

						if (at_event(pvt, s, sl - s))
						{
							goto e_cleanup;
						}

						sl += LEN(return_newline);
					}

					event_buf_used -= sl - s;
					s = sl;
				}
			}
			else
			{
				ast_debug(5, "[%s] Nothing for read\n", pvt->id);
			}
		}

e_cleanup:
		if (pvt->connected && !pvt->initialized)
		{
			ast_debug(1, "[%s] Error initializing\n", pvt->id);
			ast_verb(4,  "[%s] Error initializing\n", pvt->id);
		}

		disconnect_datacard(pvt);

		int timeout = pvt->timeout;

		pvt_unlock(pvt);

		if (!check_unloading())
		{
			sleep(timeout);
		}
	}

	return NULL;
}

static void pvt_destroy(struct datacard_pvt *pvt)
{
	ast_dsp_free(pvt->dsp);
	ast_string_field_free_memory(pvt);
	ast_free(pvt);
}

static struct datacard_pvt *load_datacard(struct ast_config *cfg, const char *cat)
{
	struct datacard_pvt	*pvt;
	const char		*audio_tty;
	const char		*data_tty;
	const char		*event_tty;
	struct ast_variable	*v;
	char			db_value[256];

	ast_debug(1, "[%s] Reading config\n", cat);

	data_tty  = ast_variable_retrieve(cfg, cat, "data");
	audio_tty = ast_variable_retrieve(cfg, cat, "audio");
	event_tty = ast_variable_retrieve(cfg, cat, "event");

	if (ast_strlen_zero(audio_tty) || ast_strlen_zero(data_tty) || ast_strlen_zero(event_tty))
	{
		ast_log(LOG_ERROR, "[%s] Skipping datacard. Missing required audio or data or event setting\n", cat);
		return NULL;
	}

	/* create and initialize our pvt structure */

	pvt = ast_calloc(1, sizeof(struct datacard_pvt));
	if (pvt == NULL)
	{
		ast_log(LOG_ERROR, "[%s] Skipping datacard. Error allocating memory\n", cat);
		return NULL;
	}

	if (ast_string_field_init(pvt, 32))
	{
		ast_log(LOG_ERROR, "[%s] Skipping datacard. String field allocation failed\n", cat);
		pvt_destroy(pvt);
		return NULL;
	}

	ast_mutex_init(&pvt->lock);
	at_buf_init(&pvt->data_at_buf, pvt->data_at_entry, ARRAY_LEN(pvt->data_at_entry));
	rb_init(&pvt->audio_write_rb, pvt->audio_write_buf, sizeof(pvt->audio_write_buf));
	pvt_reset(pvt);

	/* populate the pvt structure */

	ast_string_field_set(pvt, id,        cat);
	ast_string_field_set(pvt, data_tty,  data_tty);
	ast_string_field_set(pvt, audio_tty, audio_tty);
	ast_string_field_set(pvt, event_tty, event_tty);

	/* set some defaults */

	pvt->thread			= AST_PTHREADT_NULL;
	pvt->audio_timer		= NULL; // not need for calloc()
	pvt->callingpres		= -1;

	ast_string_field_set(pvt, context,  "default");
	ast_string_field_set(pvt, language, default_language);

	/* setup the dsp */

	pvt->dsp = ast_dsp_new();

	if (pvt->dsp == NULL)
	{
		ast_log(LOG_ERROR, "[%s] Skipping datacard. Error setting up dsp for dtmf detection\n", cat);
		pvt_destroy(pvt);
		return NULL;
	}

	ast_dsp_set_features(pvt->dsp, DSP_FEATURE_DIGIT_DETECT);

	if (relaxdtmf_global)
	{
		ast_dsp_set_digitmode(pvt->dsp, DSP_DIGITMODE_DTMF | DSP_DIGITMODE_RELAXDTMF);
	}

	for (v = ast_variable_browse(cfg, cat); v; v = v->next)
	{
		if (!strcasecmp(v->name, "context"))
		{
			ast_string_field_set(pvt, context, v->value);
		}
		else if (!strcasecmp(v->name, "language"))
		{
			ast_string_field_set(pvt, language, v->value);
		}
		else if (!strcasecmp(v->name, "group"))
		{
			pvt->group = (int) strtol(v->value, (char **) NULL, 10);		/* group is set to 0 if invalid */
		}
		else if (!strcasecmp(v->name, "rxgain"))
		{
			pvt->rxgain = (int) strtol(v->value, (char **) NULL, 10);		/* rxgain is set to 0 if invalid */
		}
		else if (!strcasecmp(v->name, "txgain"))
		{
			pvt->txgain = (int) strtol(v->value, (char **) NULL, 10);		/* txgain is set to 0 if invalid */
		}
		else if (!strcasecmp(v->name, "usecallingpres"))
		{
			pvt->usecallingpres = ast_true(v->value);				/* usecallingpres is set to 0 if invalid */
		}
		else if (!strcasecmp(v->name, "callingpres"))
		{
			pvt->callingpres = ast_parse_caller_presentation(v->value);
			if (pvt->callingpres == -1)
			{
				errno = 0;
				pvt->callingpres = (int) strtol(v->value, (char **) NULL, 10);	/* callingpres is set to -1 if invalid */
				if (pvt->callingpres == 0 && errno == EINVAL)
				{
					pvt->callingpres = -1;
				}
			}
		}
		else if (!strcasecmp(v->name, "setvar"))
		{
			pvt->vars = parse_setvar(v->value, pvt->vars);				/* add channel variable to datacard */
		}
	}

	if (!ast_db_get("Datacard/offline", pvt->id, db_value, sizeof(db_value)))
	{
		pvt->offline = ast_true(db_value);
	}

	ast_debug(1, "[%s] Datacard config loaded\n", pvt->id);

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_INSERT_TAIL(&datacards, pvt, entry);
	AST_RWLIST_UNLOCK(&datacards);

	return pvt;
}

static int load_config()
{
	struct ast_config	*cfg;
	const char		*cat;
	struct ast_variable	*v;
	struct ast_flags	config_flags = { 0 };

	cfg = ast_config_load(config, config_flags);
	if (cfg == NULL)
	{
		ast_log(LOG_WARNING, "No such configuration file %s\n", config);
		return -1;
	}
	else if (cfg == CONFIG_STATUS_FILEINVALID)
	{
		ast_log(LOG_ERROR, "Config file %s is in an invalid format.\n", config);
		return -1;
	}

	/* parse [general] section */
	for (v = ast_variable_browse(cfg, "general"); v; v = v->next)
	{
		/* handle jb conf */
		if (!ast_jb_read_conf(&jbconf_global, v->name, v->value))
		{
			continue;
		}

		if (!strcasecmp(v->name, "interval"))
		{
			errno = 0;
			discovery_interval = (int) strtol(v->value, (char **) NULL, 10);
			if (discovery_interval == 0 && errno == EINVAL)
			{
				ast_log(LOG_NOTICE, "Error parsing 'interval' in general section, using default value\n");
				discovery_interval = DISCOVERY_INT;
			}
		}
		else if (!strcasecmp(v->name, "language"))
		{
			ast_copy_string(default_language, v->value, sizeof(default_language));
		}
		else if (!strcasecmp(v->name, "relaxdtmf"))
		{
			relaxdtmf_global = ast_true(v->value);
		}
	}

	/* now load datacards */
	for (cat = ast_category_browse(cfg, NULL); cat; cat = ast_category_browse(cfg, cat))
	{
		if (strcasecmp(cat, "general"))
		{
			load_datacard(cfg, cat);
		}
	}

	ast_config_destroy(cfg);

	return 0;
}

static int check_unloading()
{
	int res;

	ast_mutex_lock(&unload_mtx);
	res = unload_flag;
	ast_mutex_unlock(&unload_mtx);

	return res;
}

static int load_module()
{
	struct datacard_pvt *pvt;

	memmove(&jbconf_global, &jbconf_default, sizeof(jbconf_global));

	if (load_config())
	{
		ast_log(LOG_ERROR, "Errors reading config file '%s', module not loaded\n", config);
		return AST_MODULE_LOAD_DECLINE;
	}

	memset(silence_frame, 0, sizeof(silence_frame));

	datacard_tech.capabilities = ast_format_cap_alloc(AST_FORMAT_CAP_FLAG_DEFAULT);
        if (datacard_tech.capabilities == NULL)
	{
		return AST_MODULE_LOAD_FAILURE;
	}
	ast_format_cap_append(datacard_tech.capabilities, ast_format_slin, 0);

	if (ast_channel_register(&datacard_tech))
	{
		ast_log(LOG_ERROR, "Unable to register channel class 'Datacard', module not loaded\n");
		goto return_error_channel;
	}

	if (ast_cli_register_multiple(datacard_cli, ARRAY_LEN(datacard_cli)))
	{
		goto return_error_cli;
	}

	if (ast_manager_register("DatacardShowDevices",
		EVENT_FLAG_SYSTEM | EVENT_FLAG_CONFIG | EVENT_FLAG_REPORTING,
		manager_action_show_devices, "List Datacard devices."))
	{
		goto return_error_manager;
	}

	if (ast_manager_register("DatacardShowStatus",
		EVENT_FLAG_SYSTEM | EVENT_FLAG_CONFIG | EVENT_FLAG_REPORTING,
		manager_action_show_status, "List status Datacard devices."))
	{
		goto return_error_manager;
	}

	if (ast_manager_register("DatacardSendUSSD",
		EVENT_FLAG_SYSTEM | EVENT_FLAG_CONFIG | EVENT_FLAG_REPORTING,
		manager_action_send_ussd, "Send a ussd command to the datacard."))
	{
		goto return_error_manager;
	}

	if (ast_manager_register("DatacardSendSMS",
		EVENT_FLAG_SYSTEM | EVENT_FLAG_CONFIG | EVENT_FLAG_REPORTING,
		manager_action_send_sms, "Send a sms message."))
	{
		goto return_error_manager;
	}
	
	if (ast_manager_register("DatacardReboot",
		EVENT_FLAG_SYSTEM | EVENT_FLAG_CONFIG | EVENT_FLAG_REPORTING,
		manager_action_reboot, "Reboot a datacard."))
	{
		goto return_error_manager;
	}

	AST_RWLIST_RDLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		if (ast_pthread_create_background(&pvt->thread, NULL, datacard_monitor, pvt) < 0)
		{
			ast_log(LOG_ERROR, "[%s] Unable to create thread\n", pvt->id);
			pvt->thread = AST_PTHREADT_NULL;
		}
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	manager_event(EVENT_FLAG_SYSTEM, "DatacardLoad", "Status: module loaded\r\n");

	return AST_MODULE_LOAD_SUCCESS;

return_error_manager:
	ast_manager_unregister("DatacardShowDevices");
	ast_manager_unregister("DatacardShowStatus");
	ast_manager_unregister("DatacardSendUSSD");
	ast_manager_unregister("DatacardSendSMS");
	ast_manager_unregister("DatacardReboot");

return_error_cli:
	ast_cli_unregister_multiple(datacard_cli, ARRAY_LEN(datacard_cli));

return_error_channel:
	ast_channel_unregister(&datacard_tech);

	return AST_MODULE_LOAD_FAILURE;
}

static int unload_module()
{
	struct datacard_pvt *pvt;

	ast_mutex_lock(&unload_mtx);
	unload_flag = 1;
	ast_mutex_unlock(&unload_mtx);

	ast_channel_unregister(&datacard_tech);
	ast_cli_unregister_multiple(datacard_cli, ARRAY_LEN(datacard_cli));

	ast_manager_unregister("DatacardShowDevices");
	ast_manager_unregister("DatacardShowStatus");
	ast_manager_unregister("DatacardSendUSSD");
	ast_manager_unregister("DatacardSendSMS");
	ast_manager_unregister("DatacardReboot");

	AST_RWLIST_WRLOCK(&datacards);
	while ((pvt = AST_RWLIST_REMOVE_HEAD(&datacards, entry)))
	{
		if (pvt->thread != AST_PTHREADT_NULL)
		{
			pthread_kill(pvt->thread, SIGURG);
			pthread_join(pvt->thread, NULL);
		}

		pvt_destroy(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	ao2_cleanup(datacard_tech.capabilities);
	datacard_tech.capabilities = NULL;

	manager_event(EVENT_FLAG_SYSTEM, "DatacardUnload", "Status: module unloaded\r\n");

	return 0;
}

AST_MODULE_INFO_STANDARD(ASTERISK_GPL_KEY, "Datacard Channel Driver");
