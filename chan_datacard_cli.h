/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static char *cli_version		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_show_all		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_show			(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_online_all		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_online			(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_offline_all		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_offline		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_cmd			(struct ast_cli_entry *, int, struct ast_cli_args *);
//static char *cli_sms			(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_ussd_all		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_ussd			(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_ccwa_disable_all	(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_ccwa_disable		(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_reboot			(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_pin_disable_all	(struct ast_cli_entry *, int, struct ast_cli_args *);
static char *cli_pin_disable		(struct ast_cli_entry *, int, struct ast_cli_args *);

static struct ast_cli_entry datacard_cli[] = {
	AST_CLI_DEFINE(cli_version,		"Show Datacard version"),
	AST_CLI_DEFINE(cli_show_all,		"Show datacard status table"),
	AST_CLI_DEFINE(cli_show,		"Show datacard state and config"),
	AST_CLI_DEFINE(cli_online_all,		"Make all datacard online"),
	AST_CLI_DEFINE(cli_online,		"Make datacard online"),
	AST_CLI_DEFINE(cli_offline_all,		"Make all datacard offline"),
	AST_CLI_DEFINE(cli_offline,		"Make device offline"),
	AST_CLI_DEFINE(cli_cmd,			"Send commands to port for debugging"),
	AST_CLI_DEFINE(cli_ussd_all,		"Send USSD commands to all datacard"),
	AST_CLI_DEFINE(cli_ussd,		"Send USSD commands to datacard"),
	AST_CLI_DEFINE(cli_ccwa_disable_all,	"Disable Call-Waiting on all datacard"),
	AST_CLI_DEFINE(cli_ccwa_disable,	"Disable Call-Waiting on datacard"),
	AST_CLI_DEFINE(cli_reboot,		"Reboot datacard"),
	AST_CLI_DEFINE(cli_pin_disable_all,	"Disable PIN code on all datacard"),
	AST_CLI_DEFINE(cli_pin_disable,		"Disable PIN code"),
};

/*
	AST_CLI_DEFINE(cli_sms,			"Send SMS from the datacard"),
*/
