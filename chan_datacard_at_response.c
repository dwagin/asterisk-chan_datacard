/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_response(struct datacard_pvt *pvt, struct at_entry *e, char *str, size_t len)
{
	static struct
	{
		at_response_t r;
		const char *str;
		size_t len;
	}
	responses_list[] = {

		{ RES_OK,		STR_AND_LEN("OK\r"),			}, // 880
		{ RES_COPS,		STR_AND_LEN("+COPS:"),			}, // 56
		{ RES_CREG,		STR_AND_LEN("+CREG:"),			}, // 56

		{ RES_CPIN,		STR_AND_LEN("+CPIN:"),			}, // 28 init
		{ RES_CNUM,		STR_AND_LEN("+CNUM:"),			}, // 28 init
		{ RES_CVOICE,		STR_AND_LEN("^CVOICE:"),		}, // 28 init
		{ RES_CPMS,		STR_AND_LEN("+CPMS:"),			}, // 28 init
		{ RES_CSQ,		STR_AND_LEN("+CSQ:"),			}, // 28 init

		{ RES_ERROR,		STR_AND_LEN("ERROR\r"),			}, // 12
		{ RES_ERROR,		STR_AND_LEN("+CME ERROR:"),		},
		{ RES_ERROR,		STR_AND_LEN("+CMS ERROR:"),		},


		{ RES_BUSY,		STR_AND_LEN("BUSY\r"),			}, // ???
		{ RES_NO_CARRIER,	STR_AND_LEN("NO CARRIER\r"),		}, // ???
		{ RES_NO_DIALTONE,	STR_AND_LEN("NO DIALTONE\r"),		}, // ???

		{ RES_CNUM,		STR_AND_LEN("ERROR+CNUM:"),		}, // ???
		{ RES_ERROR,		STR_AND_LEN("COMMAND NOT SUPPORT\r"),	}, // ???

		{ RES_CMGR,		STR_AND_LEN("+CMGR:"),			},
		{ RES_SMS_PROMPT,	STR_AND_LEN(" >"),			},

		{ RES_ERROR,		STR_AND_LEN("\r\n+CMS ERROR:"),		}, // need check reality

		{ RES_PARSE_ERROR,	STR_AND_LEN("\r"),			},
		{ RES_PARSE_ERROR,	STR_AND_LEN("^"),			},
	};

	size_t i;
	at_response_t at_res = RES_UNKNOWN;

	ast_debug(1, "[%s] Response: [%.*s]\n", pvt->id, (int) len, str);

	for (i = 0; i < ARRAY_LEN(responses_list); i++)
	{
		if (!strncmp(str, responses_list[i].str, responses_list[i].len))
		{
			at_res = responses_list[i].r;
			break;
		}
	}

	switch (at_res)
	{
		case RES_OK:
			return at_response_ok(pvt, e);

		case RES_ERROR:
			return at_response_error(pvt, e);

		case RES_CPIN:
			if (at_response_cpin(pvt, str))
			{
				at_buf_reset(&pvt->data_at_buf);
			}
			break;

		case RES_CNUM:
			at_response_cnum(pvt, str, len);
			break;

		case RES_COPS:
			at_response_cops(pvt, str, len);
			break;

		case RES_CREG:
			at_response_creg(pvt, str, len);
			break;

		case RES_CVOICE:
			at_response_cvoice(pvt, str, len);
			break;

		case RES_CPMS:
			break;

		case RES_CSQ:
			return at_response_csq(pvt, str);

		case RES_BUSY:
		case RES_NO_CARRIER:
		case RES_NO_DIALTONE:
			break;

		case RES_CMGR:
			return at_response_cmgr(pvt, str, len);

		case RES_SMS_PROMPT:
			return at_response_sms_prompt(pvt, e);

		case RES_UNKNOWN:
			switch (e->command)
			{
				case CMD_AT_CGMI:
					return at_response_cgmi(pvt, str);

				case CMD_AT_CGMM:
					return at_response_cgmm(pvt, str);

				case CMD_AT_CGMR:
					return at_response_cgmr(pvt, str);

				case CMD_AT_CGSN:
					return at_response_cgsn(pvt, str);

				case CMD_AT_CIMI:
					return at_response_cimi(pvt, str);

				default:
					trimnr(str, &len, "\r\n");
					ast_log(LOG_WARNING, "[%s] Ignoring unknown response: [%.*s]\n", pvt->id, (int) len, str);
					break;
			}

			break;

		case RES_PARSE_ERROR:
			ast_log(LOG_WARNING, "[%s] Strange response: [%.*s]\n", pvt->id, (int) len, str);
			return -1;
	}

	return 0;
}


static int at_response_ok(struct datacard_pvt *pvt, struct at_entry *e)
{
	if (e->response == RES_OK)
	{
		switch (e->command)
		{
			case CMD_AT_Z:
			case CMD_AT_E:
			case CMD_AT_COPS_INIT:
			case CMD_AT_CREG_INIT:
			case CMD_AT_CMGF:
			case CMD_AT_CPMS:
			case CMD_AT_CMGD:
			case CMD_AT_CSQ:
				break;

			case CMD_AT_CMEE:
				if (!pvt->initialized)
				{
					return (at_send_cgmi(pvt) || at_send_cgmm(pvt) || at_send_cgmr(pvt) || at_send_cgsn(pvt) || at_send_u2diag0(pvt));
				}

				break;

			case CMD_AT_U2DIAG:
				if (!pvt->initialized)
				{
					return (at_send_cpin_test(pvt) || at_send_cimi(pvt) || at_send_cnum(pvt) || at_send_clip(pvt));
				}

				break;

			case CMD_AT_CLIP:
				if (!pvt->initialized)
				{
					return (at_send_cops_init(pvt) || at_send_creg_init(pvt) || at_send_creg(pvt) || at_send_stsf(pvt));
				}

				break;

			case CMD_AT_STSF:
				if (!pvt->initialized)
				{
					return (at_send_cvoice_test(pvt) || at_send_cssn(pvt));
				}

				break;

			case CMD_AT_CSSN:
				if (!pvt->initialized)
				{
					return (at_send_cmgf(pvt) || at_send_cpms(pvt) || at_send_cscs_ucs2(pvt) || at_send_cnmi(pvt));
				}

				break;

			case CMD_AT_CSCS:
				ast_debug(1, "[%s] UCS-2 text encoding enabled\n", pvt->id);

				pvt->use_ucs2_encoding = 1;

				break;

			case CMD_AT_CNMI:
				ast_debug(1, "[%s] Datacard has SMS support\n", pvt->id);

				pvt->has_sms = 1;

				if (!pvt->initialized)
				{
					return at_send_curc(pvt);
				}

				break;

			case CMD_AT_CURC:
				if (!pvt->initialized)
				{
					if (at_send_cmgd(pvt, 1, 4) || at_send_csq(pvt))
					//if (at_send_csq(pvt))
					{
						return -1;
					}

					pvt->initialized = 1;

					manager_event_status(pvt);

					ast_debug(1, "[%s] Datacard initialized and ready\n", pvt->id);
					ast_verb(4, "[%s] Datacard initialized and ready\n", pvt->id);
				}

				break;

			case CMD_AT_A:
				if (at_send_ddsetex(pvt))
				{
					return -1;
				}

				if (pvt->audio_timer)
				{
					ast_timer_set_rate(pvt->audio_timer, 50);
				}

				break;

			case CMD_AT_DDSETEX:
				at_send_clvl(pvt, 1);
				at_send_clvl(pvt, 5);
				break;

			case CMD_AT_CFUN:
			case CMD_AT_CHUP:
			case CMD_AT_CLVL:
			case CMD_AT_COPS:
			case CMD_AT_CPIN:
			case CMD_AT_CUSD:
			case CMD_AT_D:
			case CMD_AT_DTMF:
				break;

			case CMD_AT_CLCK:
				ast_verb(1, "[%s] PIN code disabled\n", pvt->id);
				pvt->timeout = 3;
				return -1;

			case CMD_AT_CCWA:
				ast_verb(1, "[%s] Call-Waiting disabled\n", pvt->id);
				break;

			default:
				trimnr(e->buf, &e->len, "\r\n");
				ast_log(LOG_ERROR, "[%s] Received 'OK' for unhandled command [%.*s]\n", pvt->id, (int) e->len, e->buf);
				break;
		}
	}
	else
	{
		trimnr(e->buf, &e->len, "\r\n");
		ast_log(LOG_ERROR, "[%s] Received unexpected 'OK' for command [%.*s]\n", pvt->id, (int) e->len, e->buf);
	}

	return 0;
}

static int at_response_error(struct datacard_pvt *pvt, struct at_entry *e)
{
	if (e->response == RES_OK || e->response == RES_ERROR)
	{
		switch (e->command)
		{
			case CMD_AT_Z:
			case CMD_AT_E:
			case CMD_AT_CMEE:
				trimnr(e->buf, &e->len, "\r\n");
				ast_log(LOG_ERROR, "[%s] Received 'ERROR' for command [%.*s]\n", pvt->id, (int) e->len, e->buf);
				return -1;

			case CMD_AT_CGMI:
			case CMD_AT_CGMM:
			case CMD_AT_CGMR:
			case CMD_AT_CGSN:
			case CMD_AT_U2DIAG:
				ast_log(LOG_ERROR, "[%s] HW check error\n", pvt->id);
				pvt->timeout = 60;
				return -1;

			case CMD_AT_CPIN_TEST:
			case CMD_AT_CIMI:
			case CMD_AT_CNUM:
			case CMD_AT_CLIP: // maybe after gsm reg set ???
				ast_debug(1, "[%s] The SIM card is not ready\n", pvt->id);
				ast_verb(4, "[%s] The SIM card is not ready\n", pvt->id);
				pvt->timeout = 30;
				return -1;

			case CMD_AT_COPS_INIT:
			case CMD_AT_CREG_INIT:
				ast_log(LOG_WARNING, "[%s] Error set GSM options\n", pvt->id);
				pvt->timeout = 10;
				return -1;

			case CMD_AT_STSF:
				ast_debug(1, "[%s] Error disable SIM toolkit messages\n", pvt->id);

				if (!pvt->initialized)
				{
					return (at_send_cvoice_test(pvt) || at_send_cssn(pvt));
				}

				break;

			case CMD_AT_CVOICE:
				ast_debug(1, "[%s] Datacard has not support voice\n", pvt->id);
				pvt->has_voice = 0;
				break;

			case CMD_AT_CSSN:
				ast_log(LOG_WARNING, "[%s] Error activation Supplementary Service Notification\n", pvt->id);
				pvt->timeout = 10;
				return -1;

			case CMD_AT_CMGF:
			case CMD_AT_CPMS:
				ast_debug(1, "[%s] No SMS support\n", pvt->id);
				pvt->has_sms = 0;
				break;

			case CMD_AT_CSCS:
				ast_debug(1, "[%s] No UCS-2 encoding support\n", pvt->id);
				pvt->use_ucs2_encoding = 0;
				break;

			case CMD_AT_CNMI:
				ast_debug(1, "[%s] No SMS support\n", pvt->id);

				pvt->has_sms = 0;

				if (!pvt->initialized && (!pvt->has_voice || at_send_curc(pvt)))
				{
					return -1;
				}

				break;

			case CMD_AT_CURC:
				ast_log(LOG_ERROR, "[%s] Impossible to work without service messages\n", pvt->id);
				pvt->timeout = 90;
				return -1;

			case CMD_AT_CFUN:
			case CMD_AT_CLCK:
			case CMD_AT_CLIR:
			case CMD_AT_CLVL:
			case CMD_AT_COPS:
			case CMD_AT_CPIN:
			case CMD_AT_CREG:
			case CMD_AT_DTMF:
				trimnr(e->buf, &e->len, "\r\n");
				ast_log(LOG_WARNING, "[%s] Received 'ERROR' for command [%.*s]\n", pvt->id, (int) e->len, e->buf);
				break;

			case CMD_AT_A:
				ast_log(LOG_ERROR, "[%s] Answer failed\n", pvt->id);
				channel_queue_hangup(pvt, 0);
				break;

			case CMD_AT_D:
				ast_log(LOG_ERROR, "[%s] Dial failed\n", pvt->id);
				pvt->outgoing = 0;
				pvt->needchup = 0;
				manager_event_status(pvt);
				channel_queue_control(pvt, AST_CONTROL_CONGESTION);
				break;

			case CMD_AT_DDSETEX:
				ast_log(LOG_WARNING, "[%s] Error set audio port (AT^DDSETEX)\n", pvt->id);
				break;

			case CMD_AT_CHUP:
				ast_log(LOG_WARNING, "[%s] Error hangup (AT+CHUP)\n", pvt->id);
				break;

			case CMD_AT_CUSD:
				ast_log(LOG_WARNING, "[%s] Error USSD request\n", pvt->id);
				break;

			case CMD_AT_CMGR:
				ast_log(LOG_WARNING, "[%s] Error reading SMS message\n", pvt->id);
				pvt->incoming_sms = 0;
				break;

			case CMD_AT_CMGD:
				ast_log(LOG_WARNING, "[%s] Error deleting SMS message\n", pvt->id);
				pvt->incoming_sms = 0;
				break;

			case CMD_AT_CCWA:
				ast_log(LOG_WARNING, "[%s] Error Call-Waiting disable\n", pvt->id);
				break;

			default:
				trimnr(e->buf, &e->len, "\r\n");
				ast_log(LOG_ERROR, "[%s] Received 'ERROR' for unhandled command [%.*s]\n", pvt->id, (int) e->len, e->buf);
				return -1;
		}
	}
	else
	{
		trimnr(e->buf, &e->len, "\r\n");
		ast_log(LOG_ERROR, "[%s] Received unexpected 'ERROR' for command [%.*s]\n", pvt->id, (int) e->len, e->buf);
	}

	return 0;
}

static int at_response_cpin(struct datacard_pvt *pvt, char *str)
{
	if (!strncmp(str, STR_AND_LEN("+CPIN: READY")))
	{
		return 0;
	}

	if (!strncmp(str, STR_AND_LEN("+CPIN: SIM PIN")))
	{
		ast_verb(1, "[%s] Need PIN code!\n", pvt->id);
		return -1;
	}

	if (!strncmp(str, STR_AND_LEN("+CPIN: SIM PUK")))
	{
		ast_verb(1, "[%s] Need PUK code!\n", pvt->id);
		return -1;
	}

	ast_log(LOG_WARNING, "[%s] Error parsing +CPIN\n", pvt->id);

	return -1;
}

static int at_response_cnum(struct datacard_pvt *pvt, char *str, size_t len)
{
	char *number = at_parse_cnum(str, len);

	ast_string_field_set(pvt, number, number);

	if (number == NULL)
	{
		ast_log(LOG_ERROR, "[%s] Error parsing +CNUM\n", pvt->id);
		return -1;
	}

	return 0;
}


static int at_response_cops(struct datacard_pvt *pvt, char *str, size_t len)
{
	char *provider = at_parse_cops(str, len);

	ast_string_field_set(pvt, provider, provider);

	if (provider == NULL)
	{
		ast_debug(1, "[%s] Error parsing +COPS\n", pvt->id);
	}

	return -1;
}

static int at_response_creg(struct datacard_pvt *pvt, char *str, size_t len)
{
	int d;
	char *lac;
	char *ci;
	int old_gsm_status = pvt->gsm_status;

	if (at_parse_creg(str, len, &d, &pvt->gsm_status, &lac, &ci))
	{
		ast_debug(1, "[%s] Error parsing +CREG\n", pvt->id);
		return -1;
	}

	at_send_cops(pvt);

	if (d)
	{
		pvt->gsm_registered = 1;
	}
	else
	{
		pvt->gsm_registered = 0;
	}

	if (pvt->initialized && pvt->gsm_status != old_gsm_status)
	{
		manager_event_status(pvt);
	}

	if (lac)
	{
		ast_string_field_set(pvt, location_area_code, lac);
	}

	if (ci)
	{
		ast_string_field_set(pvt, cell_id, ci);
	}

	return 0;
}

static int at_response_cvoice(struct datacard_pvt *pvt, char *str, size_t len)
{
	*(strchrnul(str, '\r')) = '\0';

	/* maybe need parse [^CVOICE:0,8000,16,20] */

	pvt->has_voice = 1;

	return 0;
}

static int at_response_csq(struct datacard_pvt *pvt, char *str)
{
	pvt->rssi = -1;

	/*
	 * parse +CSQ response in the following format:
	 * +CSQ: <RSSI>,<BER>
	 */

	if (!sscanf(str, "+CSQ: %2d,", &pvt->rssi))
	{
		ast_debug(1, "[%s] Error parsing +CSQ\n", pvt->id);
		return -1;
	}

	return 0;
}

static int at_response_cmgr(struct datacard_pvt *pvt, char *str, size_t len)
{
	ssize_t res;
	char *from_number;
	char from_number_utf8[256];
	char *text;
	char text_utf8[1024];
	char text_base64[2048];

	pvt->incoming_sms = 0;

	if (at_parse_cmgr(str, len, &from_number, &text))
	{
		ast_log(LOG_ERROR, "[%s] Error parsing +CMGR\n", pvt->id);
		return 0;
	}

	if (pvt->use_ucs2_encoding)
	{
		if ((res = conv_ucs2_8bit_hexstr_to_utf8(text, strlen(text), text_utf8, sizeof(text_utf8))) > 0)
		{
			text = text_utf8;
		}
		else
		{
			ast_log(LOG_ERROR, "[%s] Error parsing SMS (convert UCS-2 to UTF-8): %s\n", pvt->id, text);
		}

		if ((res = conv_ucs2_8bit_hexstr_to_utf8(from_number, strlen(from_number), from_number_utf8, sizeof(from_number_utf8))) > 0)
		{
			from_number = from_number_utf8;
		}
		else
		{
			ast_log(LOG_ERROR, "[%s] Error parsing SMS from_number (convert UCS-2 to UTF-8): %s\n", pvt->id, from_number);
		}
	}

	ast_base64encode(text_base64, (unsigned char *) text, strlen(text), sizeof(text_base64));
	ast_debug(1, "[%s] Got SMS from %s: [%s]\n", pvt->id, from_number, text);
	ast_verb(1, "[%s] Got SMS from %s: [%s]\n", pvt->id, from_number, text);

	manager_event_new_sms_base64(pvt, from_number, text_base64);

#ifdef __ALLOW_LOCAL_CHANNELS__
	struct ast_channel *channel;

	snprintf(pvt->d_send_buf, sizeof(pvt->d_send_buf), "sms@%s", pvt->context);

	if (channel = channel_local_request(pvt, pvt->d_send_buf, pvt->id, from_number))
	{
		pbx_builtin_setvar_helper(channel, "SMS_BASE64", text_base64);

		if (ast_pbx_start(channel))
		{
			ast_log(LOG_ERROR, "[%s] Unable to start pbx on incoming sms\n", pvt->id);
			ast_hangup(channel);
		}
	}
#endif /* __ALLOW_LOCAL_CHANNELS__ */

	return 0;
}

static int at_response_sms_prompt(struct datacard_pvt *pvt, struct at_entry *e)
{
	return 0;
}








static int at_response_cgmi(struct datacard_pvt *pvt, char *str)
{
	*(strchrnul(str, '\r')) = '\0';

	ast_string_field_set(pvt, manufacturer, str);

	return 0;
}

static int at_response_cgmm(struct datacard_pvt *pvt, char *str)
{
	static const char *ussd_7bit_modems[] =
	{
		"E1550",
		"E1552",
		"E1750",
		"E150",
		"E153",
		"E160X",
		"E171",
		"E173",
	};

	static const char *ussd_ucs2_modems[] =
	{
		"K3520",
		"K3750",
		"K3765",
	};

	size_t i;

	*(strchrnul(str, '\r')) = '\0';

	ast_string_field_set(pvt, model, str);

	pvt->ussd_use_7bit_encoding = 0;
	pvt->ussd_use_ucs2_encoding = 0;
	pvt->ussd_use_ucs2_decoding = 0;

	for (i = 0; i < ARRAY_LEN(ussd_7bit_modems); i++)
	{
		if (!strcmp(pvt->model, ussd_7bit_modems[i]))
		{
			pvt->ussd_use_7bit_encoding = 1;
			pvt->ussd_use_ucs2_decoding = 0;
			break;
		}
	}

	for (i = 0; i < ARRAY_LEN(ussd_ucs2_modems); i++)
	{
		if (!strcmp(pvt->model, ussd_ucs2_modems[i]))
		{
			pvt->ussd_use_ucs2_encoding = 1;
			pvt->ussd_use_ucs2_decoding = 1;
			break;
		}
	}

	return 0;
}

static int at_response_cgmr(struct datacard_pvt *pvt, char *str)
{
	*(strchrnul(str, '\r')) = '\0';

	ast_string_field_set(pvt, firmware, str);

	return 0;
}

static int at_response_cgsn(struct datacard_pvt *pvt, char *str)
{
	*(strchrnul(str, '\r')) = '\0';

	ast_string_field_set(pvt, imei, str);

	return 0;
}

static int at_response_cimi(struct datacard_pvt *pvt, char *str)
{
	*(strchrnul(str, '\r')) = '\0';

	ast_string_field_set(pvt, imsi, str);

	return 0;
}
