/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static int at_response_ok		(struct datacard_pvt *, struct at_entry *);
static int at_response_error		(struct datacard_pvt *, struct at_entry *);

static int at_response_cpin		(struct datacard_pvt *, char *);
static int at_response_cnum		(struct datacard_pvt *, char *, size_t);

static int at_response_cops		(struct datacard_pvt *, char *, size_t);
static int at_response_creg		(struct datacard_pvt *, char *, size_t);

static int at_response_cvoice		(struct datacard_pvt *, char *, size_t);

static int at_response_csq		(struct datacard_pvt *, char *);

static int at_response_cmgr		(struct datacard_pvt *, char *, size_t);
static int at_response_sms_prompt	(struct datacard_pvt *, struct at_entry *);

static int at_response_cgmi		(struct datacard_pvt *, char *);
static int at_response_cgmm		(struct datacard_pvt *, char *);
static int at_response_cgmr		(struct datacard_pvt *, char *);
static int at_response_cgsn		(struct datacard_pvt *, char *);
static int at_response_cimi		(struct datacard_pvt *, char *);
