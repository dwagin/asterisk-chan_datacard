/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static char *at_parse_cnum(char *str, size_t len)
{
	size_t	i;
	int	state;
	char*	number = NULL;

	/*
	 * parse CNUM response in the following format:
	 * +CNUM: "<name>","<number>",<type>
	 */

	for (i = 0, state = 0; i < len && state < 5; i++)
	{
		switch (state)
		{
			case 0: /* search for start of the name (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 1: /* search for the end of the name (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 2: /* search for the start of the number (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 3: /* mark the number */
				number = &str[i];
				state++;
				/* fall through */

			case 4: /* search for the end of the number (") */
				if (str[i] == '"')
				{
					str[i] = '\0';
					state++;
				}
				break;
		}
	}

	if (state != 5)
	{
		return NULL;
	}

	return number;
}

static char *at_parse_cops(char *str, size_t len)
{
	size_t	i;
	int	state;
	char	*provider = NULL;

	/*
	 * parse COPS response in the following format:
	 * +COPS: <mode>[,<format>,"<provider>"]
	 */

	for (i = 0, state = 0; i < len && state < 3; i++)
	{
		switch (state)
		{
			case 0: /* search for start of the provider name (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 1: /* mark the provider name */
				provider = &str[i];
				state++;
				/* fall through */

			case 2: /* search for the end of the provider name (") */
				if (str[i] == '"')
				{
					str[i] = '\0';
					state++;
				}
				break;
		}
	}

	if (state != 3)
	{
		return NULL;
	}

	return provider;
}

static int at_parse_creg(char *str, size_t len, int *gsm_reg, int *gsm_status, char **lac, char **ci)
{
	size_t	i;
	int	state;
	char*	p1 = NULL;
	char*	p2 = NULL;
	char*	p3 = NULL;
	char*	p4 = NULL;

	*gsm_reg = 0;
	*gsm_status = -1;
	*lac = NULL;
	*ci  = NULL;

	/*
	 * parse CREG response in the following format:
	 * +CREG: [<p1>,]<p2>[,<p3>,<p4>]
	 */

	for (i = 0, state = 0; i < len && state < 8; i++)
	{
		switch (state)
		{
			case 0:
				if (str[i] == ':')
				{
					state++;
				}
				break;

			case 1:
				if (str[i] != ' ')
				{
					p1 = &str[i];
					state++;
				}
				/* fall through */

			case 2:
				if (str[i] == ',')
				{
					str[i] = '\0';
					state++;
				}
				break;

			case 3:
				p2 = &str[i];
				state++;
				/* fall through */

			case 4:
				if (str[i] == ',')
				{
					str[i] = '\0';
					state++;
				}
				break;

			case 5:
				if (str[i] != ' ')
				{
					p3 = &str[i];
					state++;
				}
				/* fall through */

			case 6:
				if (str[i] == ',')
				{
					str[i] = '\0';
					state++;
				}
				break;

			case 7:
				if (str[i] != ' ')
				{
					p4 = &str[i];
					state++;
				}
				break;
		}
	}

	if (state < 2)
	{
		return -1;
	}

	if ((p2 && !p3 && !p4) || (p2 && p3 && p4))
	{
		p1 = p2;
	}

	if (p1)
	{
		errno = 0;

		*gsm_status = (int) strtol(p1, (char **) NULL, 10);

		if (*gsm_status == 0 && errno == EINVAL)
		{
			*gsm_status = -1;
			return -1;
		}

		if (*gsm_status == 1 || *gsm_status == 5)
		{
			*gsm_reg = 1;
		}
	}

	if (p2 && p3 && !p4)
	{
		*lac = p2;
		*ci  = p3;
	}
	else if (p3 && p4)
	{
		*lac = p3;
		*ci  = p4;
	}

	return 0;
}

static char *at_parse_clip(char *str, size_t len)
{
	size_t	i;
	int	state;
	char	*clip = NULL;

	/*
	 * parse clip info in the following format:
	 * +CLIP: "123456789",128, ...
	 */

	for (i = 0, state = 0; i < len && state < 3; i++)
	{
		switch (state)
		{
			case 0: /* search for start of the number (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 1: /* mark the number */
				clip = &str[i];
				state++;
				/* fall through */

			case 2: /* search for the end of the number (") */
				if (str[i] == '"')
				{
					str[i] = '\0';
					state++;
				}
				break;
		}
	}

	if (state != 3)
	{
		return NULL;
	}

	return clip;
}

static int at_parse_cusd(char *str, size_t len, int *type, char **cusd, int *dcs)
{
	size_t i;
	int state;
	char *p1 = NULL;
	char *p2 = NULL;

	*type = -1;
	*cusd = NULL;
	*dcs  = -1;

	/*
	 * parse cusd message in the following format:
	 * +CUSD: 0,"100,00 EURO, valid till 01.01.2010, you are using tariff "Mega Tariff". More informations *111#.",15
	 */

	for (i = 0, state = 0; i < len && state < 8; i++)
	{
		switch (state)
		{
			case 0:
				if (str[i] == ':')
				{
					state++;
				}
				break;

			case 1:
				if (str[i] != ' ')
				{
					p1 = &str[i];
					state++;
				}
				/* fall through */

			case 2:
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 3:
				*cusd = &str[i];
				state++;
				break;

			case 4:
				if (str[i] == '"')
				{
					str[i] = '\0';
					state++;
				}
				break;

			case 5:
				if (str[i] == ',')
				{
					state++;
				}
				break;

			case 6:
				p2 = &str[i];
				state++;
				break;

			case 7:
				if (str[i] == '\r')
				{
					str[i] = '\0';
					state++;
				}
				break;
		}
	}

	if (state < 7)
	{
		return -1;
	}

	errno = 0;
	*type = (int) strtol(p1, (char **) NULL, 10);

	if (errno == EINVAL)
	{
		return -1;
	}

	errno = 0;
	*dcs = (int) strtol(p2, (char **) NULL, 10);

	if (errno == EINVAL)
	{
		return -1;
	}

	return 0;
}

static int at_parse_cmgr(char* str, size_t len, char** number, char** text)
{
	size_t i;
	int state;

	*number = NULL;
	*text   = NULL;

	/*
	 * parse cmgr info in the following format:
	 * +CMGR: <msg status>,"+123456789",...\r\n
	 * <message text>\r\n
	 */

	for (i = 0, state = 0; i < len && state < 7; i++)
	{
		switch (state)
		{
			case 0: /* search for start of the number section (,) */
				if (str[i] == ',')
				{
					state++;
				}
				break;

			case 1: /* find the opening quote (") */
				if (str[i] == '"')
				{
					state++;
				}
				break;

			case 2: /* mark the start of the number */
				*number = &str[i];
				state++;
				break;

			/* fall through */

			case 3: /* search for the end of the number (") */
				if (str[i] == '"')
				{
					str[i] = '\0';
					state++;
				}
				break;

			case 4: /* search for the start of the message text (\n) */
				if (str[i] == '\n')
				{
					state++;
				}
				break;

			case 5: /* mark the start of the message text */
				*text = &str[i];
				state++;
				break;

			case 6:
				if (str[i] == '\r')
				{
					str[i] = '\0';
					state++;
				}
				break;
		}
	}

	if (state != 7)
	{
		return -1;
	}

	return 0;
}
