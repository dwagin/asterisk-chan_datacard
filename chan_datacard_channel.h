/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static struct ast_channel	*channel_new		(struct datacard_pvt *, int, const char *, const char *, const char *, const char *, const struct ast_assigned_ids *, const struct ast_channel *);
static struct ast_channel	*channel_request	(const char *, struct ast_format_cap *, const struct ast_assigned_ids *, const struct ast_channel *, const char *, int *);
static int			channel_call		(struct ast_channel *, const char *, int);
static int			channel_hangup		(struct ast_channel *);
static int			channel_answer		(struct ast_channel *);
static int			channel_digit_begin	(struct ast_channel *, char);
static int			channel_digit_end	(struct ast_channel *, char, unsigned int);
static struct ast_frame		*channel_read		(struct ast_channel *);
static inline void		channel_timing_write	(struct datacard_pvt *);
static int			channel_write		(struct ast_channel *, struct ast_frame *);
static int			channel_fixup		(struct ast_channel *, struct ast_channel *);
static int			channel_devicestate	(const char *);
static int			channel_indicate	(struct ast_channel *, int, const void *, size_t);

static int			channel_queue_control	(struct datacard_pvt *, enum ast_control_frame_type);
static int			channel_queue_hangup	(struct datacard_pvt *, int);

#ifdef __ALLOW_LOCAL_CHANNELS__
static struct ast_channel	*channel_local_request	(struct datacard_pvt *, void *, const char *, const char *);
#endif

static struct ast_channel_tech datacard_tech =
{
	.type			= "Datacard",
	.description		= "Datacard Channel Driver",
	.requester		= channel_request,
	.call			= channel_call,
	.hangup			= channel_hangup,
	.answer			= channel_answer,
	.send_digit_begin	= channel_digit_begin,
	.send_digit_end		= channel_digit_end,
	.read			= channel_read,
	.write			= channel_write,
	.exception		= channel_read,
	.fixup			= channel_fixup,
	.devicestate		= channel_devicestate,
	.indicate		= channel_indicate
};

struct ast_format_cap *chan_datacard_format_cap;
