/*
 * Copyright (C) 2009 - 2014
 *
 * Artem Makhutov <artem@makhutov.org>
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

static char *cli_version(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard version";
			e->usage   =	"Usage: datacard version\n"
					"       Shows version.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 2)
	{
		return CLI_SHOWUSAGE;
	}

	ast_cli(a->fd, "\nDatacard - %s\n\n", DATACARD_VERSION);

	return CLI_SUCCESS;
}

static char *cli_show_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt;
	char buf[32];

#define FORMAT1 "%-10.10s %-5.5s %-19.19s %-19.19s %-14.14s %-6.6s %-16.16s %-15.15s %-15.15s %-14.14s\n"
#define FORMAT2 "%-10.10s %-5d %-19.19s %-19.19s %-14.14s %-6.6s %-16.16s %-15.15s %-15.15s %-14.14s\n"

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard show all";
			e->usage   =	"Usage: datacard show all\n"
					"       Show Datacard status table.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	ast_cli(a->fd, FORMAT1, "ID", "Group", "Status", "Signal strength", "Provider", "Model", "Firmware", "IMEI", "IMSI", "Number");

	AST_RWLIST_RDLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		ast_cli(a->fd, FORMAT2,
			pvt->id,
			pvt->group,
			datacard_status_txt(pvt),
			rssi2dBm(pvt->rssi, buf, sizeof(buf)),
			pvt->provider,
			pvt->model,
			pvt->firmware,
			pvt->imei,
			pvt->imsi,
			pvt->number
		);
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

#undef FORMAT1
#undef FORMAT2

	return CLI_SUCCESS;
}

static char *cli_show(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard show";
			e->usage   =	"Usage: datacard show <datacard>\n"
					"       Shows the state and config of datacard.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n", a->argv[2]);
	}
	else
	{
		pvt_lock(pvt);
		ast_cli(a->fd, "\n Current datacard settings:\n");
		ast_cli(a->fd, "------------------------------------\n");
		ast_cli(a->fd, "  Datacard                : %s\n", pvt->id);
		ast_cli(a->fd, "  Context                 : %s\n", pvt->context);
		ast_cli(a->fd, "  Language                : %s\n", pvt->language);
		ast_cli(a->fd, "  Group                   : %d\n", pvt->group);
		ast_cli(a->fd, "  GSM status              : %s\n", datacard_gsm_status_txt(pvt));
		ast_cli(a->fd, "  Status                  : %s\n", datacard_status_txt(pvt));
		ast_cli(a->fd, "  Voice                   : %s\n", (pvt->has_voice) ? "Yes" : "No");
		ast_cli(a->fd, "  SMS                     : %s\n", (pvt->has_sms) ? "Yes" : "No");
		ast_cli(a->fd, "  RSSI                    : %d\n", pvt->rssi);
		ast_cli(a->fd, "  Mode                    : %d\n", pvt->linkmode);
		ast_cli(a->fd, "  Submode                 : %d\n", pvt->linksubmode);
		ast_cli(a->fd, "  Provider                : %s\n", pvt->provider);
		ast_cli(a->fd, "  Manufacturer            : %s\n", pvt->manufacturer);
		ast_cli(a->fd, "  Model                   : %s\n", pvt->model);
		ast_cli(a->fd, "  Firmware                : %s\n", pvt->firmware);
		ast_cli(a->fd, "  IMEI                    : %s\n", pvt->imei);
		ast_cli(a->fd, "  IMSI                    : %s\n", pvt->imsi);
		ast_cli(a->fd, "  Number                  : %s\n", pvt->number);
		ast_cli(a->fd, "  Use CallingPres         : %s\n", pvt->usecallingpres ? "Yes" : "No");
		ast_cli(a->fd, "  Default CallingPres     : %s\n", pvt->callingpres < 0 ? "<Not set>" : ast_describe_caller_presentation(pvt->callingpres));
		ast_cli(a->fd, "  Use UCS-2 encoding      : %s\n", pvt->use_ucs2_encoding ? "Yes" : "No");
		ast_cli(a->fd, "  USSD use 7 bit encoding : %s\n", pvt->ussd_use_7bit_encoding ? "Yes" : "No");
		ast_cli(a->fd, "  USSD use UCS-2 encoding : %s\n", pvt->ussd_use_ucs2_encoding ? "Yes" : "No");
		ast_cli(a->fd, "  USSD use UCS-2 decoding : %s\n", pvt->ussd_use_ucs2_decoding ? "Yes" : "No");
		ast_cli(a->fd, "  Location area code      : %s\n", pvt->location_area_code);
		ast_cli(a->fd, "  Cell ID                 : %s\n", pvt->cell_id);

		if (pvt->vars)
		{
			struct ast_variable *var = NULL;
			char varbuf[512];

			ast_cli(a->fd, "  Variables:\n");
			for (var = pvt->vars; var; var = var->next)
			{
				ast_cli(a->fd, "   %s=%s\n", var->name, ast_get_encoded_str(var->value, varbuf, sizeof(varbuf)));
			}
		}

		ast_cli(a->fd, "\n");
		pvt_unlock(pvt);
	}

	return CLI_SUCCESS;
}

static char *cli_online_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard online all";
			e->usage   =	"Usage: datacard online all\n"
					"       Make all datacard online.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		pvt->offline = 0;
		ast_db_del("Datacard/offline", pvt->id);
		manager_event_status(pvt);
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	return CLI_SUCCESS;
}

static char *cli_online(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard online";
			e->usage   =	"Usage: datacard online <datacard>\n"
					"       Make <datacard> online.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n\n", a->argv[2]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	pvt->offline = 0;
	ast_db_del("Datacard/offline", pvt->id);
	manager_event_status(pvt);
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

static char *cli_offline_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard offline all";
			e->usage   =	"Usage: datacard offline all\n"
					"       Make all datacard offline.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		pvt->offline = 1;
		ast_db_put("Datacard/offline", pvt->id, "yes");
		manager_event_status(pvt);
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	return CLI_SUCCESS;
}

static char *cli_offline(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard offline";
			e->usage   =	"Usage: datacard offline <datacard>\n"
					"       Make <datacard> offline.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n\n", a->argv[2]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	pvt->offline = 1;
	ast_db_put("Datacard/offline", pvt->id, "yes");
	manager_event_status(pvt);
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

static char *cli_cmd(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard cmd";
			e->usage   =	"Usage: datacard cmd <datacard> <command>\n"
					"       Send <command> on the datacard\n"
					"       with the specified <datacard>.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 4)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n\n", a->argv[2]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	if (pvt->connected)
	{
		char buf[512];
		size_t len;

		len = snprintf(buf, sizeof(buf), "%s\r", a->argv[3]);

		if (at_send_buf(pvt, CMD_UNKNOWN, RES_OK, buf, MIN(len, sizeof(buf))))
		{
			ast_cli(a->fd, "[%s] Error sending command: [%s]\n", pvt->id, a->argv[3]);
		}
	}
	else
	{
		ast_cli(a->fd, "[%s] Datacard not connected\n", pvt->id);
	}
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

/*
static char *cli_sms(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	return CLI_SUCCESS;
}
*/

static char *cli_ussd_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command = "datacard ussd all";
			e->usage =
				"Usage: datacard ussd all <command>\n"
				"       Send ussd <command> on all datacard.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 4)
	{
		return CLI_SHOWUSAGE;
	}

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		if (can_ussd(pvt))
		{
			if (at_send_cusd(pvt, a->argv[3]))
			{
				ast_cli(a->fd, "[%s] Error sending USSD request\n", pvt->id);
			}
		}
		else
		{
			ast_cli(a->fd, "[%s] Datacard wrong state\n", pvt->id);
		}
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	return CLI_SUCCESS;
}

static char *cli_ussd(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command = "datacard ussd";
			e->usage =
				"Usage: datacard ussd <datacard> <command>\n"
				"       Send ussd <command> on the datacard\n"
				"       with the specified <datacard>.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 4)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli (a->fd, "Datacard [%s] not found\n", a->argv[2]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	if (can_ussd(pvt))
	{
		if (at_send_cusd(pvt, a->argv[3]))
		{
			ast_cli(a->fd, "[%s] Error sending USSD request\n", pvt->id);
		}
	}
	else
	{
		ast_cli(a->fd, "[%s] Datacard wrong state\n", pvt->id);
	}
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

static char *cli_ccwa_disable_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command = "datacard ccwa disable all";
			e->usage =
				"Usage: datacard ccwa disable all\n"
				"       Disable Call-Waiting on all datacard\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 4)
	{
		return CLI_SHOWUSAGE;
	}

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		if (can_ussd(pvt))
		{
			if (at_send_ccwa_disable(pvt))
			{
				ast_cli(a->fd, "[%s] Error sending CCWA command\n", pvt->id);
			}
		}
		else
		{
			ast_cli(a->fd, "[%s] Datacard wrong state\n", pvt->id);
		}
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	return CLI_SUCCESS;
}

static char *cli_ccwa_disable(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command = "datacard ccwa disable";
			e->usage =
				"Usage: datacard ccwa disable <datacard>\n"
				"       Disable Call-Waiting on <datacard>\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 3)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 4)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[3]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n", a->argv[3]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	if (can_ussd(pvt))
	{
		if (at_send_ccwa_disable(pvt))
		{
			ast_cli(a->fd, "[%s] Error sending CCWA command\n", pvt->id);
		}
	}
	else
	{
		ast_cli(a->fd, "[%s] Datacard wrong state\n", pvt->id);
	}
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

static char *cli_reboot(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command = "datacard reboot";
			e->usage =
				"Usage: datacard reboot <datacard>\n"
				"       Reboot datacard <datacard>\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 2)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 3)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[2]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n", a->argv[2]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	if (pvt->connected)
	{
		if (at_send_cfun_reboot(pvt))
		{
			ast_cli(a->fd, "[%s] Error sending reboot command\n", pvt->id);
		}
		else
		{
			pvt->incoming = 1;
		}
	}
	else
	{
		ast_cli(a->fd, "[%s] Datacard not connected\n", pvt->id);
	}
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}

static char *cli_pin_disable_all(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard pin disable all";
			e->usage   =	"Usage: datacard pin disable all <pin>\n"
					"       Disable PIN code on all datacard.\n";
			return NULL;

		case CLI_GENERATE:
			return NULL;
	}

	if (a->argc < 5)
	{
		return CLI_SHOWUSAGE;
	}

	AST_RWLIST_WRLOCK(&datacards);
	AST_RWLIST_TRAVERSE(&datacards, pvt, entry)
	{
		pvt_lock(pvt);
		if (pvt->connected && !pvt->initialized)
		{
			if (at_send_cpin(pvt, a->argv[4]) || at_send_clck(pvt, a->argv[4]))
			{
				ast_cli(a->fd, "[%s] Error disable PIN code\n", pvt->id);
			}
		}
		else
		{
			ast_cli(a->fd, "[%s] Datacard wrong state\n", pvt->id);
		}
		pvt_unlock(pvt);
	}
	AST_RWLIST_UNLOCK(&datacards);

	return CLI_SUCCESS;
}

static char *cli_pin_disable(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	struct datacard_pvt *pvt = NULL;

	switch (cmd)
	{
		case CLI_INIT:
			e->command =	"datacard pin disable";
			e->usage   =	"Usage: datacard pin disable <datacard> <pin>\n"
					"       Disable PIN code on datacard.\n";
			return NULL;

		case CLI_GENERATE:
			if (a->pos == 3)
			{
				return complete_datacard(a->line, a->word, a->pos, a->n, 0);
			}
			return NULL;
	}

	if (a->argc < 5)
	{
		return CLI_SHOWUSAGE;
	}

	pvt = find_datacard(a->argv[3]);
	if (pvt == NULL)
	{
		ast_cli(a->fd, "Datacard [%s] not found\n\n", a->argv[3]);
		return CLI_SUCCESS;
	}

	pvt_lock(pvt);
	if (pvt->connected)
	{
		if (at_send_cpin(pvt, a->argv[4]) || at_send_clck(pvt, a->argv[4]))
		{
			ast_cli(a->fd, "[%s] Error disable PIN code\n", pvt->id);
		}
	}
	else
	{
		ast_cli(a->fd, "[%s] Datacard not connected\n", pvt->id);
	}
	pvt_unlock(pvt);

	return CLI_SUCCESS;
}
